const SEC = 1000;
class Global{
	static taskId;
	static isDebug = true;
}

class Task{
	constructor(name){
		this.name = name;
		this.time = 0;
		this.isRun = true;
	}
	
	toString(){
		return JSON.stringify(this);
	}
	
	ftime(){
		let htime = Math.floor(this.time/3600);
		let mtime = Math.floor((this.time - htime*3600)/60);
		let stime = this.time - htime*3600 - mtime*60;
		return ((htime<10)?"0"+htime:htime)+":"+((mtime<10)?"0"+mtime:mtime)+":"+((stime<10)?"0"+stime:stime);
	}
	
	addLink(link){
		let checkPrefix = link.match(/^http[s]{0,1}:\/\/|^ftp[s]{0,1}:\/\/|^mailto:\/\//g);
		if(checkPrefix){
			this.link = link.replace(checkPrefix[0], '');
			this.schema = checkPrefix[0];
		}else{
			this.link = link;
			this.schema = "https://";
		}
	}
	
	getLink(){
		if( !this.link == ""){
			return this.schema + this.link;
		}
	}
	
	toCsv(){
		return this.name+","+this.getLink()+","+this.ftime()+","+this.time+"\n";
	}

}

class Storage{

	static tasks;
	
	static add(task){
		if( !localStorage.getItem("tasks") ){
			this.tasks = new Map();
		}else{
			this.tasks = Storage.readAll();
		}
		let id = this.tasks.size;
		task.id = id;
		localStorage.setItem("task-"+id,task);
		this.tasks.set(id, task);
		localStorage.setItem("tasks", this.tasks.size);
		if(Global.isDebug){console.log("Storage.add(task)",task);}
		
		return task;
	}
	
	static addArray(tasks){
		if(Global.isDebug){ console.log("Storage.addArray(tasks)", tasks); }
		if(tasks.length > 0){
			tasks.forEach(function(item) {
				Storage.add(item);
			});
		}
	}
	
	static read(id){
		let task = new Task();
		Object.assign(task, JSON.parse(localStorage.getItem("task-"+id)));
		if(Global.isDebug){ console.log("Storage.read(id)",id,task); }
		return task;
	}
	
	static remove(id){
		if(Global.isDebug){console.log("Storage.remove(id)",id);}
		let lastTaskId = localStorage.getItem("tasks");
		for(let i=0; i<lastTaskId; i++){
			if(i>id){
				let updatedTask = this.read(i);
				updatedTask.id = i-1;
				this.update(updatedTask);
			}
		}
		lastTaskId--;
		localStorage.removeItem("task-"+lastTaskId);
		localStorage.setItem("tasks", lastTaskId);
		
		return id;
	}
	
	static readAll(){
		this.tasks = new Map();
		if(localStorage.getItem("tasks")){
			for(let i=0; i<localStorage.getItem("tasks"); i++){
				this.tasks.set(i, this.read(i));
			}
		}
		return this.tasks;
	}
	static removeAll(){}
	static update(task){
		console.log("Storage.update(task)",task);
		localStorage.setItem("task-"+task.id,task);
		
		return task;
	}
}

class Timer{

	static getCurrentSec(){
	
		let now = new Date();
		let currentSec = Math.round( now.getTime()/SEC );
		if(Global.isDebug){ console.log("Timer.getCurrentSec()", currentSec); }
			
		return currentSec;
	}

	static updateTasks(step){
		let totalTime = 0;
		let atLeastRun = false;
		if(Global.isDebug){ console.log("Timer.updateTasks(step)", step); }
		if(!step){step = 1}
		let tasks = Storage.readAll();
		for(let i=0; i<tasks.size; i++){
			let task = tasks.get(i);
			totalTime += task.time;
			if(task.isRun){
				task.time += step;
				atLeastRun = true;
				Storage.update(task);
				Gui.updateTask(task);
			}
		}
		
		let totalTasksEl = document.getElementById("totalTasks");
		let totalTimeEl = document.getElementById("totalTime");
		totalTasksEl.textContent = tasks.size;
		totalTimeEl.textContent = Timer.secToFtime(totalTime);
		
		if( tasks.size > 0){
			if(atLeastRun){
				Timer.changeIcon("run");
			}else{
				Timer.changeIcon("stop");
			}
		}else{
			Timer.changeIcon("nope");
		}
		
		localStorage.setItem("timestamp",Timer.getCurrentSec());
		
	}
	
	static wakeUpTimer(){
		if(localStorage.getItem("timestamp")){
			let howLongSlept = Math.round(Timer.getCurrentSec()-parseInt(localStorage.getItem("timestamp")));
			this.updateTasks(howLongSlept);
		}
	}
	
	static ftimeToSec(ftime){
		return parseInt(ftime.split(":")[0])*3600+parseInt(ftime.split(":")[1])*60+parseInt(ftime.split(":")[2]);
	}
	
	static secToFtime(sec){
		let htime = Math.floor(sec/3600);
		let mtime = Math.floor((sec - htime*3600)/60);
		let stime = sec - htime*3600 - mtime*60;
		return ((htime<10)?"0"+htime:htime)+":"+((mtime<10)?"0"+mtime:mtime)+":"+((stime<10)?"0"+stime:stime);
	}
	
	static changeIcon(status){
		console.log("change icon: ", status);
		switch(status){
			case "run":
				chrome.browserAction.setIcon({path:"icons/49run.png"});
				break;
			case "stop":
				chrome.browserAction.setIcon({path:"icons/49stop.png"});
				break;
			case "nope":
				chrome.browserAction.setIcon({path:"icons/49.png"});
				break;
		}
	}
	
}

class Gui{

	static draw(task){
		if(Global.isDebug){ console.log("Gui.draw(task)", task);}
		let id = task.id;
		let templateTask = document.getElementById("task");
		let newTask = templateTask.cloneNode(true);
		newTask.setAttribute("id","task-"+id); 
		document.getElementById("tasks").appendChild(newTask);
		let names = ["copyName", "copyLink", "taskName", "taskLink", "taskTimer", "taskControl", "taskEdit" ];
		for(let i=0; i<names.length; i++){
			let el = document.getElementsByName(names[i])[1];
			el.id = names[i]+"-"+id;
			el.removeAttribute("name");
		}
		document.getElementById("taskName-"+id).textContent = task.name;
		if(task.time != "0"){
			document.getElementById("taskTimer-"+id).textContent = task.ftime();
		}
		let link = document.getElementById("taskLink-"+id);
		link.textContent = task.link;
		link.href = task.getLink();
		
		let taskEdit = document.getElementById("taskEdit-"+id);
		if(taskEdit){
			taskEdit.addEventListener("click", editTask, true);
		}
		let icon = document.getElementById("taskControl-"+id);
		if( !task.isRun){
			icon.src="icons/pause.png";
			icon.classList.remove("blink");
		}
		icon.addEventListener("click", playStopTask, true);
		let copyLink = document.getElementById("copyLink-"+id);
		copyLink.addEventListener("click", copyBuffer, true);
		if( !task.link){
			copyLink.style.display = "none";
		}
		
	}
	
	static drawAll(tasks){
		if(Global.isDebug){ console.log("Gui.drawAll(tasks)", tasks);}
		for(let i=0; i<tasks.size; i++){
			this.draw(tasks.get(i));
		}
		return tasks;
	}
	
	static updateTask(task){
		if(Global.isDebug) console.log("Gui.updateTask(task)", task);
		let id = task.id;
		let name = document.getElementById("taskName-"+id);
		let time = document.getElementById("taskTimer-"+id);
		let link = document.getElementById("taskLink-"+id);
		let control = document.getElementById("taskControl-"+id);
		let icon = document.getElementById("taskControl-"+id);
		if(task.isRun){
			playIcon(icon);
		}else{
			pauseIcon(icon);
		}
		name.textContent = task.name;
		time.textContent = task.ftime();
		link.textContent = task.link;
		link.href = task.getLink();
		
	}
	
	static remove(id){
		if(Global.isDebug){console.log("Gui.remove(id)", id);}		
		document.getElementById("task-"+id).remove();
	}
	
	static removeAll(){}
	static timeTik(){}
}

class Legacy{

	static getOldTasks(){
	
		var oldTasks = new Array();
		if(localStorage.getItem("list")){ 
			
			let list = localStorage.getItem("list").split("|");
			for(let i=0;i<list.length; i++){
				if(list[i]){
					let time = localStorage.getItem("time-"+list[i]);
					let task = localStorage.getItem("protask-"+list[i]);
					let status = localStorage.getItem("ctrl_btn-"+list[i]);
					let lnk = (localStorage.getItem("link-"+list[i]))?localStorage.getItem("link-"+list[i]):"";
					let proj = (localStorage.getItem("proj-"+list[i]))?localStorage.getItem("proj-"+list[i]):"";
					let oldName = proj==""?task:proj+" : "+task;
					let oldTask = new Task(oldName);
					oldTask.link = lnk;
					oldTask.time = time==null?0:Timer.ftimeToSec(time);
					oldTask.isRun = status==1?true:false;
					oldTasks.push(oldTask);
				}
			}
			localStorage.clear();
		}
		return oldTasks;
	}
}

function createTask(){
	let name = document.getElementById("name").value;
	let link = document.getElementById("link").value;
	if(name){
		let task = new Task(name);
		if(link){
			task.addLink(link);
		}
		Gui.draw(Storage.add(task));
	}
	toggleCreatePanel();
}

function openSettings(){
	window.open("options.html?app","_self");
}

function editTask(){
	let id = this.id.split('-')[1];
	Global.taskId = id;
	let task = Storage.read(id);
	task.top = document.getElementById("task-"+id).getBoundingClientRect().y;
	showEditingPanel(task);
}

function deleteTask(){
	Storage.remove(Global.taskId);
	let tasks = Storage.readAll();
	for(let i=0; i<tasks.size; i++){
		if(i>=Global.taskId) {
			Gui.updateTask(tasks.get(i));
		}
	}
	Gui.remove(tasks.size);
	hideEditingPanel();
}

function showEditingPanel(task){
	if(Global.isDebug){ console.log("showEditingPanel(task)", task);}
	let bodyRect = document.body.getBoundingClientRect()
	let editingPanel = document.getElementById('editingPanel');
	let editName = document.getElementById('editName');
	let editTime = document.getElementById('editTime');
	let editLink = document.getElementById('editLink');
	editName.value = task.name;
	editTime.value = task.ftime();
	if(task.link && task.link != ""){
		editLink.value = task.getLink();
	}
	
	editingPanel.style.top = (task.top - bodyRect.top)+"px";
	let bottomLine = editingPanel.getBoundingClientRect().height + task.top;
	if( bottomLine > bodyRect.bottom) {
		document.body.style.height = bottomLine+"px";
	}else{
		document.body.style.height = null;
	}
	editingPanel.style.left = 0;
	editingPanel.style.right = 0;
}

function hideEditingPanel(){
	let editingPanel = document.getElementById('editingPanel');
	editingPanel.style.left = '-100%';
	editingPanel.style.right = '100%';
	document.body.style.height = null;
}

function saveEditingPanel(){
	let editName = document.getElementById('editName');
	let editLink = document.getElementById('editLink');
	let editTime = document.getElementById('editTime');
	let task = new Task(editName.value, editTime.value);
	task.addLink(editLink.value);
	task.id = Global.taskId;
	let origTask = Storage.read(Global.taskId);
	task.isRun = origTask.isRun;
	let checkedTime = editTime.value.match(/[0-9]{2,3}:[0-9]{2}:[0-9]{2}/g);
	if(checkedTime){
		task.time = Timer.ftimeToSec(checkedTime[0]);
	}else{
		task.time = origTask.time;
	}
	
	Gui.updateTask(Storage.update(task));
	hideEditingPanel();
}

function toggleCreatePanel(){
	var addingPanel = document.getElementById("addingPanel");
	if (addingPanel.style.maxHeight){
      addingPanel.style.maxHeight = null;
    } else {
      addingPanel.style.maxHeight = addingPanel.scrollHeight + "px";
    }
}

function playStopTask(){

	let id = this.id.split('-')[1];
	let task = Storage.read(id);
	let icon = document.getElementById("taskControl-"+id);
	if(task.isRun){
		task.isRun = false;
		pauseIcon(icon);
	}else{
		task.isRun = true;
		playIcon(icon);
	}
	Gui.updateTask(Storage.update(task));
	
}

function playIcon(icon){
	icon.src="icons/play.png";
	if( !icon.classList.contains("blink") ){ icon.classList.add("blink"); }
}

function pauseIcon(icon){
	icon.src="icons/pause.png";
	icon.classList.remove("blink");
}

function copyBuffer(){
	let id = this.id.split('-')[1];
	let link = document.getElementById("taskLink-"+id);
	var textArea = document.createElement("textarea");
	textArea.value = link.innerHTML;
	document.body.appendChild(textArea);
	textArea.focus();
	textArea.select();

	try {
	var successful = document.execCommand('copy');
	var msg = successful ? 'successful' : 'unsuccessful';
	if(Global.isDebug){ console.log('Copying text command was ' + msg); }
	} catch (err) {
	if(Global.isDebug){ console.log('Oops, unable to copy'); }
	}

	document.body.removeChild(textArea);
	
}

function exportToCSV(){
	
	var tasks = Storage.readAll();
	var textToSave = "Name,Link,Time,Sec\n"
	for(var i=0;i<tasks.size; i++){
		textToSave += tasks.get(i).toCsv();
	}
	
    var textToSaveAsBlob = new Blob([textToSave], {type:"text/plain"});
    var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
    var fileNameToSaveAs = "myWork.csv";
 
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    downloadLink.href = textToSaveAsURL;
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
 	if(Global.isDebug){ console.log("exportToCSV()", textToSaveAsURL);}
 		
    downloadLink.click();
}

function destroyClickedElement(event)
{
    document.body.removeChild(event.target);
}

function reset(){
	document.getElementById("editTime").value = "00:00:00";	
}

window.onload=function(){
	document.getElementById("add").addEventListener("click",toggleCreatePanel);
	document.getElementById("createTask").addEventListener("click",createTask);
	document.getElementById("cancel").addEventListener("click", toggleCreatePanel);
	document.getElementById("undo").addEventListener("click", hideEditingPanel);
	document.getElementById("save").addEventListener("click", saveEditingPanel);
	document.getElementById("deleteTask").addEventListener("click", deleteTask);
	document.getElementById("exportCSV").addEventListener("click", exportToCSV, true);
	document.getElementById("reset").addEventListener("click", reset, true);
		
	Storage.addArray(Legacy.getOldTasks());
	
	if (Gui.drawAll(Storage.readAll()).size == 0){
		Timer.changeIcon("nope");
	}
	
	Timer.wakeUpTimer();
	
}

window.setInterval(Timer.updateTasks,SEC);